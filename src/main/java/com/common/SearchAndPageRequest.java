package com.common;

import com.constants.Constants;
import lombok.Data;

/**
 * 查询分页公共请求对象
 * +----------------------------------------------------------------------
 * | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 * | Copyright (c) 2016~2020 https://www.crmeb.com All rights reserved.
 * +----------------------------------------------------------------------
 * | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
 * +----------------------------------------------------------------------
 * | Author: CRMEB Team <admin@crmeb.com>
 * +----------------------------------------------------------------------
 */
@Data
public class SearchAndPageRequest {

    private String keywords;

    private int page = Constants.DEFAULT_PAGE;

    private int limit = Constants.DEFAULT_LIMIT;

    private Integer type;
}
